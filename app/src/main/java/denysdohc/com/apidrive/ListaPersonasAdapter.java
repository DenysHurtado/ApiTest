package denysdohc.com.apidrive;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import denysdohc.com.apidrive.Models.Persona;

public class ListaPersonasAdapter extends RecyclerView.Adapter<ListaPersonasAdapter.PersonasViewHolder>{

    ArrayList<Persona> listaPersona;

    public ListaPersonasAdapter(ArrayList<Persona> listaPersona) {
        this.listaPersona = listaPersona;
    }

    @Override
    public PersonasViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_personas,null,false);
        return new PersonasViewHolder(item);
    }

    @Override
    public void onBindViewHolder(ListaPersonasAdapter.PersonasViewHolder holder, int position) {
        holder.documento.setText(listaPersona.get(position).getId().toString());
        holder.nombres.setText(listaPersona.get(position).getNombres());
    }

    @Override
    public int getItemCount() {
        return listaPersona.size();
    }

    public class PersonasViewHolder extends RecyclerView.ViewHolder {

        TextView documento,nombres;

        public PersonasViewHolder(View itemView) {
            super(itemView);

            documento = (TextView) itemView.findViewById(R.id.textDocumento);
            nombres = (TextView) itemView.findViewById(R.id.textNombre);
        }
    }
}
