package denysdohc.com.apidrive.Utilidades;

/**
 * Created by laptop on 21/03/2018.
 */

public class Utilidades {
    //Constantes campos tabla persona
    public static final String TABLA_PERSONA ="persona";
    public static final String CAMPO_ID="id";
    public static final String CAMPO_NOMBRES="nombres";

    public static final String CREAR_TABLA_PERSONA ="CREATE TABLE " +
            ""+ TABLA_PERSONA +" ("+CAMPO_ID+" " +
            "INTEGER, "+CAMPO_NOMBRES+" TEXT)";

}
