package denysdohc.com.apidrive;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.drive.Drive;

import java.util.ArrayList;

import denysdohc.com.apidrive.Models.Persona;
import denysdohc.com.apidrive.Utilidades.Utilidades;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button registrar;
    private Button autentificar;
    private Button hacer_backup;
    private Button recuperar_backup;
    private EditText nombres,id;
    private GoogleSignInClient mGoogleSignInClient;

    ConexionSQLite conexion;

    ArrayList<Persona> listaPersonas;
    RecyclerView recycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGoogleSignInClient = buildGoogleSignInClient();


        setContentView(R.layout.activity_main);
        conexion = new ConexionSQLite(this,"bd_personas",null,1);
        initialize();
        registrar.setOnClickListener(this);


    }

    private GoogleSignInClient buildGoogleSignInClient() {
        GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(Drive.SCOPE_FILE)
                .build();
        return GoogleSignIn.getClient(this, signInOptions);
    }


    public void initialize() {
        registrar = (Button) findViewById(R.id.button_registrar);
        autentificar = (Button) findViewById(R.id.button_autentificar);
        hacer_backup = (Button) findViewById(R.id.button_hacer_backup);
        recuperar_backup = (Button) findViewById(R.id.button_recuperar_backup);
        nombres = findViewById(R.id.editText_nombres);
        id = findViewById(R.id.editText_id);

        recycler = (RecyclerView) findViewById(R.id.recycler_lista);
        recycler.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        listaPersonas = new ArrayList<>();
        consultarListaPersonas();

        ListaPersonasAdapter adapter=new ListaPersonasAdapter(listaPersonas);
        recycler.setAdapter(adapter);
//        recycler.setLayoutManager(new GridLayoutManager(this,2));

    }

    private void consultarListaPersonas() {
        SQLiteDatabase db=conexion.getReadableDatabase();

        Persona persona = null;
        //select * from personas
        Cursor cursor=db.rawQuery("SELECT * FROM "+ Utilidades.TABLA_PERSONA,null);

        while (cursor.moveToNext()){
            persona=new Persona();
            persona.setId(cursor.getInt(0));
            persona.setNombres(cursor.getString(1));

            listaPersonas.add(persona);
        }
        db.close();
    }

    @Override
    public void onClick(View view) {
        registrarPersona();
    }

    private void registrarPersona() {

        SQLiteDatabase db = conexion.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Utilidades.CAMPO_ID,id.getText().toString());
        values.put(Utilidades.CAMPO_NOMBRES,nombres.getText().toString());

        Long idResultante=db.insert(Utilidades.TABLA_PERSONA,Utilidades.CAMPO_ID,values);
        Toast.makeText(this,"Id Registro: "+idResultante,Toast.LENGTH_SHORT).show();
        db.close();
    }
}
